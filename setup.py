""" MicrobeGPS setup.py script. Allows direct installation of the tool or creating Python eggs. """

try:
	import PyQt4
except ImportError:
	print('PyQt4 is not installed on your system! Please install PyQt4 first. You can download PyQt4 from http://www.riverbankcomputing.co.uk/software/pyqt/download or use your package manager to install the library.')
	import sys
	sys.exit()

from microbegps import __version__
from setuptools import setup, find_packages

setup(
    name = "MicrobeGPS",
    version = __version__,

    author = "Martin S. Lindner",
    author_email = "lindnerm@rki.de",
    description = "MicrobeGPS is a powerful taxonomic profiling tool for metagenomic data.",
    license = "BSD",
    keywords = "Metagenomics Taxonomic Profiling",
    url = "",

    packages = find_packages(),
    include_package_data = True,
    install_requires = ['numpy','scipy','matplotlib','matplotlib_venn'],
    zip_safe = True,
    entry_points = {"console_scripts": ["MicrobeGPS = microbegps.gui:main",],}
)
